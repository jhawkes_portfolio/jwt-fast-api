# jwt-fast-api

## Name
JWT Authentication App Using React and FastAPI

## Description
This is a demonstration app to authenticate users before allowing them to see a protected page.

The app utilizes a JWT token to authenticate the user
The backend is using FastAPI for endpoints and SQLAlchemy for database management

The frontend is using React and Redux for state management